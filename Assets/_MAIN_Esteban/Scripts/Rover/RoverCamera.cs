﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NMS
{
    namespace Rover
    {
        [System.Serializable]
        public class RoverCamera
        {
            public string cameraName;
            public Camera camera;
        }
    }
}