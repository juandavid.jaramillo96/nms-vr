﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using VRTK;
using TMPro;

namespace NMS
{
    namespace Rover
    {
        public class CamerasController : MonoBehaviour
        {
            #region Properties

            [SerializeField, FoldoutGroup("Rover cameras")]
            private List<RoverCamera> roverCameras;

            [SerializeField]
            private TextMeshProUGUI camText;

            //cached objects
            private RoverCamera currentRoverCamera;

            #endregion

            #region Unity methods

            private void Awake()
            {
                GetComponents();
                Subscribe();
            }

            private void Start()
            {
                Setup();
            }

            private void Update()
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    NextCamera();
                }
                if (Input.GetKeyDown(KeyCode.Q))
                {
                    PreviousCamera();
                }
            }

            #endregion

            #region Class methods

            //Private methods

            private void GetComponents()
            {
                //...
            }

            private void Subscribe()
            {
                VRTK_FinderHelper.Instance.GetControllerEvents(VRTK_DeviceFinder.Devices.RightController).ButtonTwoPressed += OnRightButtonTwoPressed;
                VRTK_FinderHelper.Instance.GetControllerEvents(VRTK_DeviceFinder.Devices.LeftController).ButtonTwoPressed += OnLeftButtonTwoPressed;
            }

            private void OnRightButtonTwoPressed(object sender, ControllerInteractionEventArgs e)
            {
                NextCamera();
            }

            private void OnLeftButtonTwoPressed(object sender, ControllerInteractionEventArgs e)
            {
                PreviousCamera();
            }

            private void Setup()
            {
                currentRoverCamera = roverCameras[0];
                SetCamera();
            }

            private void SetCamera()
            {
                HideAllCameras();
                currentRoverCamera.camera.gameObject.SetActive(true);
                camText.text = currentRoverCamera.cameraName;
            }

            private void NextCamera()
            {
                int newIndex = roverCameras.IndexOf(currentRoverCamera) + 1 < roverCameras.Count ? roverCameras.IndexOf(currentRoverCamera) + 1 : roverCameras.IndexOf(currentRoverCamera);
                currentRoverCamera = roverCameras[newIndex];
                SetCamera();
            }

            private void PreviousCamera()
            {
                int newIndex = roverCameras.IndexOf(currentRoverCamera) - 1 > -1 ? roverCameras.IndexOf(currentRoverCamera) - 1 : 0;
                currentRoverCamera = roverCameras[newIndex];
                SetCamera();
            }

            private void HideAllCameras()
            {
                foreach (RoverCamera camera in roverCameras)
                {
                    camera.camera.gameObject.SetActive(false);
                }
            }

            //Public methods

            #endregion
        }
    }
}