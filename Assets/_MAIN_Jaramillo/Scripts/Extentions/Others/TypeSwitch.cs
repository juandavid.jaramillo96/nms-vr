///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 11/02/2019 15:40
///-----------------------------------------------------------------

// Base from: https://stackoverflow.com/questions/7252186/switch-case-on-type-c-sharp

using System;
using System.Collections.Generic;

public class TypeSwitch
{
    #region Properties

    private Dictionary<Type, Action<object>> matches = new Dictionary<Type, Action<object>>();

    #endregion

    #region Class functions

    /// <summary>
    /// Executes switch case base on the object type.
    /// </summary>
    /// <param name="obj"></param>
    public void Switch(object obj)
    {
        matches[obj.GetType()](obj);
    }

    /// <summary>
    /// Add a switch case for a specific object type.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="action"></param>
    /// <returns></returns>
    public TypeSwitch Case<T>(Action<T> action)
    {
        matches.Add(typeof(T), (obj) => action((T)obj));

        return this;
    }

    #endregion
}
