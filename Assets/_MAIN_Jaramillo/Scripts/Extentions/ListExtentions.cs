///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 15/01/2019 15:31
///-----------------------------------------------------------------

namespace System
{
    namespace Collections
    {
        namespace Generic
        {
            namespace Utils
            {
                public static class ListExtensions
                {
                    /// <summary>
                    /// Shuffles the element order of the specified list.
                    /// </summary>
                    public static IList<T> Shuffle<T>(this IList<T> list)
                    {
                        int count = list.Count;
                        int last = count - 1;

                        for (int i = 0; i < last; ++i)
                        {
                            var random = UnityEngine.Random.Range(i, count);
                            var tempElement = list[i];

                            list[i] = list[random];
                            list[random] = tempElement;
                        }

                        return list;
                    }
                }
            }
        }
    }
}
