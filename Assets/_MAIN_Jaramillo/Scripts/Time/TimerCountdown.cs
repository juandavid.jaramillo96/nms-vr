///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 24/04/2019 10:22
///-----------------------------------------------------------------

using System;
using System.Text;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
using Sirenix.OdinInspector;

public class TimerCountdown : MonoDebug
{
    #region Properties

    [Space, SerializeField]
    private float _countdownTime;

    [Space, Tooltip("If null, then there wont be text update.")]
    public TextMeshProUGUI[] timerTexts;

    [Space]
    public TimeFormat format;

    [Space, EnumToggleButtons]
    public TimeUnits units;

    [Space]
    public TimeUpdateMode timeUpdateMode;

    [Space]
    public ToStringFormat daysFormat = ToStringFormat.TwoSignificantFigures;

    public ToStringFormat hoursFormat, minutesFormat, secondsFormat, millisecondsFormat = ToStringFormat.TwoSignificantFigures;

    [Space, Tooltip("Executes timer from start."), SerializeField]
    private bool executeFromStart;

    [Header("Unity Events"), SerializeField]
    private UnityEvent onTimerStart;
    [SerializeField]
    private DynamicFloatEvent onTimerStop;
    [SerializeField]
    private UnityEvent onCountdownDone;

    // Events
    [HideInInspector]
    public Action onTimerStartAction;
    [HideInInspector]
    public Action<float> onTimerStopAction;
    [HideInInspector]
    public Action onCountdownDoneAction;

    // Getters & Setters
    [FoldoutGroup("Getters & Setters"), SerializeField]
    public bool isPaused
    {
        get; private set;
    }
    [FoldoutGroup("Getters & Setters"), SerializeField]
    public int days
    {
        get; private set;
    }
    [FoldoutGroup("Getters & Setters"), SerializeField]
    public int hours
    {
        get; private set;
    }
    [FoldoutGroup("Getters & Setters"), SerializeField]
    public int minutes
    {
        get; private set;
    }
    [FoldoutGroup("Getters & Setters"), SerializeField]
    public float seconds
    {
        get; private set;
    }
    [FoldoutGroup("Getters & Setters"), SerializeField]
    public float milliseconds
    {
        get; private set;
    }

    public float countdownTime
    {
        get
        {
            return _countdownTime;
        }
        private set
        {
            _countdownTime = value;
        }
    }

    // Time control
    public float time
    {
        get; private set;
    }

    // Coroutines
    private IEnumerator timeCoroutine;

    #endregion

    #region Unity functions

    private void OnEnable()
    {
        foreach (var timerText in timerTexts)
        {
            if (timerText != null)
                timerText.gameObject.SetActive(true);
        }
    }

    private void OnDisable()
    {
        foreach (var timerText in timerTexts)
        {
            if (timerText != null)
                timerText.gameObject.SetActive(false);
        }
    }

    #endregion

    #region Class functions

    public override void Setup()
    {
        if (executeFromStart)
            StartTimer();
    }

    // ***

    public void StartTimer()
    {
        StopTimer(false);

        Log("TimerCountdown :: StartTimer()");

        timeCoroutine = TimeCoroutine();

        // Events...
        onTimerStart?.Invoke();

        onTimerStartAction?.Invoke();

        // Coroutine start...
        StartCoroutine(timeCoroutine);
    }

    public void StopTimer(bool executeEvents = true)
    {
        if (timeCoroutine != null)
        {
            Log("TimerCountdown :: StopTimer()");

            // Coroutine stop...
            StopCoroutine(timeCoroutine);

            if (executeEvents)
            {
                // Events...
                onTimerStop?.Invoke(time);

                onTimerStopAction?.Invoke(time);
            }
        }
    }

    // ***

    public void Pause()
    {
        Log("TimerCountdown :: Pause()");

        isPaused = true;
    }

    public void Unpause()
    {
        Log("TimerCountdown :: Unpause()");

        isPaused = false;
    }

    // ***

    private void SetNormalTimeText(TextMeshProUGUI[] timerTexts)
    {
        foreach (var timerText in timerTexts)
        {
            SetNormalTimeText(timerText);
        }
    }

    private void SetNormalTimeText(TextMeshProUGUI timerText)
    {
        StringBuilder stringBuilder = new StringBuilder();

        // Estimating Days...
        BuildNormalTimeString(TimeUnits.Days, daysFormat, days, ref stringBuilder);
        // Estimating Hours...
        BuildNormalTimeString(TimeUnits.Hours, hoursFormat, hours, ref stringBuilder);
        // Estimating Minutes...
        BuildNormalTimeString(TimeUnits.Minutes, minutesFormat, minutes, ref stringBuilder);
        // Estimating Seconds...
        BuildNormalTimeString(TimeUnits.Seconds, secondsFormat, seconds, ref stringBuilder);
        // Estimating Miliseconds...
        BuildNormalTimeString(TimeUnits.Miliseconds, millisecondsFormat, milliseconds, ref stringBuilder);

        timerText.text = stringBuilder.ToString();
    }

    private void SetSexagesimalDegreesTimeText(TextMeshProUGUI[] timerTexts)
    {
        foreach (var timerText in timerTexts)
        {
            SetSexagesimalDegreesTimeText(timerText);
        }
    }

    private void SetSexagesimalDegreesTimeText(TextMeshProUGUI timerText)
    {
        StringBuilder stringBuilder = new StringBuilder();

        // Estimating Days...
        BuildNormalTimeString(TimeUnits.Days, daysFormat, days, ref stringBuilder);
        // Estimating Hours...
        BuildNormalTimeString(TimeUnits.Hours, hoursFormat, hours, ref stringBuilder);
        // Estimating Minutes...
        BuildSexagesimalTimeString(TimeUnits.Minutes, minutesFormat, minutes, ref stringBuilder, SexagesimalSigns.Minutes);
        // Estimating Seconds...
        BuildSexagesimalTimeString(TimeUnits.Seconds, secondsFormat, seconds, ref stringBuilder, SexagesimalSigns.Seconds);
        // Estimating Miliseconds...
        BuildSexagesimalTimeString(TimeUnits.Miliseconds, millisecondsFormat, milliseconds, ref stringBuilder);

        timerText.text = stringBuilder.ToString();
    }

    // ***

    private void BuildNormalTimeString(TimeUnits unit, ToStringFormat toStringFormat, float time, ref StringBuilder stringBuilder)
    {
        // Estimating time...
        if (units.HasFlag(unit))
        {
            if (!string.IsNullOrEmpty(stringBuilder.ToString()))
                stringBuilder.Append(":");

            stringBuilder.Append(toStringFormat.ToString(time));
        }
    }

    private void BuildSexagesimalTimeString(TimeUnits unit, ToStringFormat toStringFormat, float time, ref StringBuilder stringBuilder, SexagesimalSigns? sexagesimalSign = null)
    {
        if (unit == TimeUnits.Minutes ||
            unit == TimeUnits.Seconds ||
            unit == TimeUnits.Miliseconds)
        {
            // Estimating time...
            if (units.HasFlag(unit))
            {
                if (unit == TimeUnits.Minutes)
                {
                    if (!string.IsNullOrEmpty(stringBuilder.ToString()))
                        stringBuilder.Append(":");
                }

                stringBuilder.Append(toStringFormat.ToString(time));

                if (sexagesimalSign != null)
                    stringBuilder.Append((char)sexagesimalSign);
            }
        }
        else
            BuildNormalTimeString(unit, toStringFormat, time, ref stringBuilder);
    }

    #endregion

    #region Coroutines

    private IEnumerator TimeCoroutine()
    {
        time = countdownTime;

        while (isActiveAndEnabled)
        {
            while (isPaused)
            {
                yield return null;
            }

            time -= (timeUpdateMode == TimeUpdateMode.Delta) ? Time.deltaTime : Time.fixedDeltaTime;

            // Days...
            days = (int)(time / 86400); // There are 86400 seconds in a day (60 * 60 * 24)...
            // Hours...
            hours = (int)((time / 3600) % 24); // There are 3600 seconds in a hour (60 * 60)...
            // Minutes...
            minutes = (int)((time / 60) % 60); // There are 60 seconds in a minute...
            // Seconds...
            seconds = (int)((time % 60)); // ...
            // Milliseconds...
            milliseconds = ((time * 1000) % 1000); // There are 1000 milliseconds in a second...

            if (timerTexts != null)
            {
                switch (format)
                {
                    case TimeFormat.Normal:
                        SetNormalTimeText(timerTexts);
                        break;

                    case TimeFormat.Sexagesimal:
                        SetSexagesimalDegreesTimeText(timerTexts);
                        break;

                    default:
                        break;
                }
            }

            // Time-out ?
            if (time <= 0)
            {
                // Integers time units...
                days = hours = minutes = 0;
                // Float point time units...
                seconds = milliseconds = 0;

                switch (format)
                {
                    case TimeFormat.Normal:
                        SetNormalTimeText(timerTexts);
                        break;

                    case TimeFormat.Sexagesimal:
                        SetSexagesimalDegreesTimeText(timerTexts);
                        break;

                    default:
                        break;
                }

                onCountdownDone?.Invoke();

                onCountdownDoneAction?.Invoke();

                StopTimer(false);
            }

            yield return (timeUpdateMode == TimeUpdateMode.Delta) ? null : new WaitForSeconds(Time.fixedDeltaTime);
        }
    }

    #endregion
}
