///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 27/12/2018 09:32
///-----------------------------------------------------------------

using System;
using UnityEngine.Events;

namespace Collections
{
    namespace Events
    {
        [Serializable]
        public class EventWrapper
        {
            #region Properties

            public UnityEvent unityEvent;

            // Hidden
            public Action callback;

            #endregion

            #region Class functions

            public void Invoke()
            {
                unityEvent?.Invoke();

                callback?.Invoke();
            }

            #endregion
        }
    }
}
