using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Collections
{
    namespace Events
    {
        public class RandomEvent : MonoDebug
        {
            #region Properties

            [Header("Settings"), SerializeField, Range(0, 270)]
            private float from;
            [SerializeField, Range(0, 270)]
            private float to;

            [Header("Events"), SerializeField]
            public EventWrapper @event;

            [Header("Others"), SerializeField]
            private bool executeFromStart;

            // Hiden
            private float random;

            // Coroutines
            private IEnumerator randomEventCoroutine;

            #endregion

            #region Class functions

            public override void Setup()
            {
                if (executeFromStart)
                    StartRandomEvent();
            }

            // ***

            public void StartRandomEvent()
            {
                StopRandomEvent();

                Log("RandomEvent :: StartRandomEvent()");

                randomEventCoroutine = RandomEventCoroutine();

                StartCoroutine(randomEventCoroutine);
            }

            public void StopRandomEvent()
            {
                if (randomEventCoroutine != null)
                {
                    Log("RandomEvent :: StopRandomEvent()");

                    StopCoroutine(randomEventCoroutine);

                    randomEventCoroutine = null;
                }
            }

            #endregion

            #region Coroutines

            private IEnumerator RandomEventCoroutine()
            {
                while (isActiveAndEnabled)
                {
                    random = Random.Range(from, to);

                    yield return new WaitForSeconds(random);

                    @event?.Invoke();
                }
            }

            #endregion
        }
    }
}
