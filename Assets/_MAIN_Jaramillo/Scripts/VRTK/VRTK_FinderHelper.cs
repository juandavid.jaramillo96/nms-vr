using System.Collections.Generic;
using UnityEngine;

namespace VRTK
{
    public class VRTK_FinderHelper : MonoDebug
    {
        #region Properties

        [Space, SerializeField]
        private GameObject _rightController;
        [SerializeField]
        private GameObject _leftController;

        // Hidden
        [HideInInspector]
        public Dictionary<VRTK_DeviceFinder.Devices, GameObject> controllers = new Dictionary<VRTK_DeviceFinder.Devices, GameObject>();

        // Getters & Setters
        public GameObject rightController
        {
            get
            {
                return _rightController ?? GetController(VRTK_DeviceFinder.Devices.RightController);
            }
        }

        public GameObject leftController
        {
            get
            {
                return _leftController ?? GetController(VRTK_DeviceFinder.Devices.LeftController);
            }
        }

        // Singleton!
        public static VRTK_FinderHelper Instance
        {
            get; private set;
        }

        #endregion // Properties

        #region Unity functions

        protected override void Awake()
        {
            if (Instance != null)
                DestroyImmediate(this);
            else
                Instance = this;

            base.Awake();
        }

        #endregion // Unity functions

        #region Class functions

        public override void GetComponents()
        {
            base.GetComponents();

            SetControllersDictionary();
        }

        // ***

        public GameObject GetController(VRTK_DeviceFinder.Devices controller)
        {
            if (controller != VRTK_DeviceFinder.Devices.Headset)
                return controllers.ContainsKey(controller) ? controllers[controller] : VRTK_DeviceFinder.DeviceTransform(controller).gameObject;
            else
                return null;
        }

        // ***

        public VRTK_ControllerEvents GetControllerEvents(VRTK_DeviceFinder.Devices controller)
        {
            return GetControllerComponent<VRTK_ControllerEvents>(controller);
        }

        public VRTK_ControllerEvents GetControllerEvents(GameObject controller)
        {
            return GetControllerComponent<VRTK_ControllerEvents>(controller);
        }

        public VRTK_InteractTouch GetInteractTouch(VRTK_DeviceFinder.Devices controller)
        {
            return GetControllerComponent<VRTK_InteractTouch>(controller);
        }

        public VRTK_InteractTouch GetInteractTouch(GameObject controller)
        {
            return GetControllerComponent<VRTK_InteractTouch>(controller);
        }

        public VRTK_InteractUse GetInteractUse(VRTK_DeviceFinder.Devices controller)
        {
            return GetControllerComponent<VRTK_InteractUse>(controller);
        }

        public VRTK_InteractUse GetInteractUse(GameObject controller)
        {
            return GetControllerComponent<VRTK_InteractUse>(controller);
        }

        public VRTK_InteractGrab GetInteractGrab(VRTK_DeviceFinder.Devices controller)
        {
            return GetControllerComponent<VRTK_InteractGrab>(controller);
        }

        public VRTK_InteractGrab GetInteractGrab(GameObject controller)
        {
            return GetControllerComponent<VRTK_InteractGrab>(controller);
        }

        public VRTK_Pointer GetPointer(VRTK_DeviceFinder.Devices controller)
        {
            return GetControllerComponent<VRTK_Pointer>(controller);
        }

        public VRTK_Pointer GetPointer(GameObject controller)
        {
            return GetControllerComponent<VRTK_Pointer>(controller);
        }

        public VRTK_BasePointerRenderer GetRenderer(VRTK_DeviceFinder.Devices controller)
        {
            return GetControllerComponent<VRTK_BasePointerRenderer>(controller);
        }

        public VRTK_BasePointerRenderer GetRenderer(GameObject controller)
        {
            return GetControllerComponent<VRTK_BasePointerRenderer>(controller);
        }

        public VRTK_UIPointer GetUIPointer(VRTK_DeviceFinder.Devices controller)
        {
            return GetControllerComponent<VRTK_UIPointer>(controller);
        }

        public VRTK_UIPointer GetUIPointer(GameObject controller)
        {
            return GetControllerComponent<VRTK_UIPointer>(controller);
        }

        // ***

        public VRTK_BasicTeleport GetTeleport()
        {
            return FindObjectOfType<VRTK_BasicTeleport>();
        }

        // ***

        public VRTK_BodyPhysics GetBodyPhysics()
        {
            return FindObjectOfType<VRTK_BodyPhysics>();
        }

        // ***

        public T GetControllerComponent<T>(VRTK_DeviceFinder.Devices controller)where T : MonoBehaviour
        {
            return (controller != VRTK_DeviceFinder.Devices.Headset) ? GetController(controller).GetComponent<T>() : null;
        }

        public T GetControllerComponent<T>(GameObject controller) where T : MonoBehaviour
        {
            return (IsController(controller)) ? controller.GetComponent<T>() : null;
        }

        // ***

        private bool IsController(GameObject controller)
        {
            return VRTK_DeviceFinder.IsControllerRightHand(controller) || VRTK_DeviceFinder.IsControllerLeftHand(controller);
        }

        // ***

        private void SetControllersDictionary()
        {
            controllers.Add(VRTK_DeviceFinder.Devices.RightController, rightController);
            controllers.Add(VRTK_DeviceFinder.Devices.LeftController, leftController);
        }

        #endregion // Class functions
    }
}
