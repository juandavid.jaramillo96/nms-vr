///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 15/02/2019 14:01
///-----------------------------------------------------------------

using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;

public class Loading : MonoDebug
{
    #region Properties

    [FoldoutGroup("UI: Loading"), SerializeField]
    private TextMeshProUGUI loadingTitle;
    [FoldoutGroup("UI: Loading"), SerializeField]
    private TextMeshProUGUI loadingPercentage;

    [FoldoutGroup("UI: Loading"), Space, SerializeField]
    private Slider loadingSlider;

    // Cached Components
    private float progress = 0;

    #endregion

    #region Unity functions

    private void Update()
    {
        if (SceneLoader.Instance != null)
            UpdateLoadingUI();
    }

	#endregion
	
	#region Class functions

	public override void Setup()
	{
        if (progress != 0)
            progress = 0;
	}

    // ***

	private void UpdateLoadingUI()
    {
        progress = SceneLoader.Instance.loadProgress;

        if (loadingSlider != null)
            loadingSlider.value = progress;

        if (loadingPercentage != null)
            loadingPercentage.text = string.Format("{0}%", (int)(progress * 100.0f));
    }

	#endregion
}
