///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 15/02/2019 14:01
///-----------------------------------------------------------------

using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Interlude : MonoDebug
{
    #region Properties

    [Space, SerializeField]
    private bool fadeFromStart;

    [Space, SerializeField]
    private float delay;
    [SerializeField]
    private float timeToLoadNextScene, timeToWait, timeToFade;

    [Space, SerializeField]
    private LeanTweenType fadeInCurve;
    [SerializeField]
    private LeanTweenType fadeOutCurve;

    [Space, SerializeField]
    private UnityEvent onInterludeEnd;

    // Const
    private readonly int BLACK_OUT = 0;
    private readonly int WHITE_IN = 1;

    private readonly Color BLANK = new Color(Color.white.r, Color.white.g, Color.white.b, 0);

    // Coroutines
    private IEnumerator fade;

    #endregion

    #region Class functions

    public override void Setup()
    {
        if (fadeFromStart)
            StartFade();
    }

    // ***

    public void LoadNextScene()
    {
        SceneLoader.Instance.LoadNextScene();
    }

    // ***

    public void StartFade()
    {
        StopFade();

        Log("Interlude :: StartFade()");

        fade = Fade();

        StartCoroutine(fade);
    }

    public void StopFade()
    {
        if (fade != null)
        {
            Log("Interlude :: StopFade()");

            StopCoroutine(fade);
        }
    }

    #endregion

    #region Coroutines

    private IEnumerator Fade()
    {
        Image[] images = FindObjectsOfType<Image>();

        foreach (var image in images)
        {
            if (image.color.a != BLACK_OUT)
                image.color = BLANK;
        }

        yield return new WaitForSeconds(delay);

        if (images != null)
        {
            foreach (var image in images)
            {
                if (!image.CompareTag("FadeIgnore"))
                    LeanTween.alpha(image.rectTransform, WHITE_IN, timeToFade).setEase(fadeInCurve);
            }

            yield return new WaitForSeconds(timeToWait);

            foreach (var image in images)
            {
                if (!image.CompareTag("FadeIgnore"))
                    LeanTween.alpha(image.rectTransform, BLACK_OUT, timeToFade).setEase(fadeOutCurve);
            }
        }

        yield return new WaitForSeconds(timeToLoadNextScene);

        if (onInterludeEnd != null)
            onInterludeEnd.Invoke();
    }

    #endregion
}
