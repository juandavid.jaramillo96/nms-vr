///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 22/05/2019 08:00
///   Company: NMS
///-----------------------------------------------------------------

using UnityEngine;
using Sirenix.OdinInspector;

namespace NMS
{
    namespace Rover
    {
        [RequireComponent(typeof(Controller))]
        public class InputManager : MonoDebug
        {
            #region Properties

            [FoldoutGroup("General Settings"), SerializeField, Space]
            private Vector2 offset;

            [FoldoutGroup("Input Settings"), SerializeField, Space]
            private string horizontalAxisName = "Horizontal";
            [FoldoutGroup("Input Settings"), SerializeField]
            private string verticalAxisName = "Vertical";

            // Getters & Setters
            public float horizontalAxis
            {
                get; private set;
            }

            public float verticalAxis
            {
                get; private set;
            }

            // Cached Components (Rover)
            private Controller roverController;

            #endregion

            #region Unity functions

            private void FixedUpdate()
            {
                GetInputs();
                AddOffset();
                
                // Updating lift truck input axes...
                UpdateHorizontalAxis();
                UpdateVerticalAxis();
            }

            #endregion

            #region Class functions

            public override void GetComponents()
            {
                base.GetComponents();

                roverController = GetComponent<Controller>();
            }

            // ***

            private void GetInputs()
            {
                // Getting Unity inputs...
                horizontalAxis = Input.GetAxis(horizontalAxisName);
                verticalAxis = Input.GetAxis(verticalAxisName);
            }

            // ***

            private void AddOffset()
            {
                // Adding inputs offset...
                horizontalAxis += offset.x;
                verticalAxis += offset.y;
            }

            // ***

            private void UpdateHorizontalAxis()
            {
                roverController.SetHorizontal(horizontalAxis);
            }

            private void UpdateVerticalAxis()
            {
                roverController.SetVertical(verticalAxis);
            }

            #endregion
        }
    }
}
