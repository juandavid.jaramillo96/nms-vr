///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 21/05/2019 08:51
///   Company: NMS
///-----------------------------------------------------------------

using UnityEngine;
using Sirenix.OdinInspector;

namespace NMS
{
    namespace Rover
    {
        public class Controller : MonoDebug
        {
            #region Properties

            [FoldoutGroup("Wheel Settings"), SerializeField, Space]
            private WheelCollider frontLeftWheel;
            [FoldoutGroup("Wheel Settings"), SerializeField]
            private WheelCollider frontRightWheel;
            [FoldoutGroup("Wheel Settings"), SerializeField]
            private WheelCollider middleLeftWheel, middleRightWheel;
            [FoldoutGroup("Wheel Settings"), SerializeField]
            private WheelCollider rearLeftWheel, rearRightWheel;

            [FoldoutGroup("Wheel Settings"), SerializeField, Space]
            private Transform frontLeftWheelTransform;
            [FoldoutGroup("Wheel Settings"), SerializeField]
            private Transform frontRightWheelTransform;
            [FoldoutGroup("Wheel Settings"), SerializeField]
            private Transform middleLeftWheelTransform, middleRightWheelTransform;
            [FoldoutGroup("Wheel Settings"), SerializeField]
            private Transform rearLeftWheelTransform, rearRightWheelTransform;

            [FoldoutGroup("Car Settings"), SerializeField, Space]
            private float maxSteerAngle = 30;

            [FoldoutGroup("Car Settings"), SerializeField, Space]
            private float motorForce = 50;
            [FoldoutGroup("Car Settings"), SerializeField]
            private float backwardMotorForce = 25;

            // Getters & Setters
            public float currentHorizontalAxis
            {
                get; private set;
            }

            public float lastHorizontalAxis
            {
                get; private set;
            }

            public float currentVerticalAxis
            {
                get; private set;
            }

            public float lastVerticalAxis
            {
                get; private set;
            }

            public float steeringAngle
            {
                get; private set;
            }

            public float torque
            {
                get; private set;
            }

            public new Vector3 transform
            {
                get; private set;
            }

            // Cached Components
            private Vector3 position;
            private Quaternion rotation;

            #endregion

            #region Unity functions


            private void FixedUpdate()
            {
                Steer();
                Accelerate();
                UpdateWheelTransforms();
            }

            #endregion

            #region Class functions

            public void SetHorizontal(float value)
            {
                Log(string.Format("Controller :: SetHorizontal() : {0}", value));

                // Set current & last horizontal value...
                lastHorizontalAxis = currentHorizontalAxis;

                currentHorizontalAxis = value;
            }

            public void SetVertical(float value)
            {
                Log(string.Format("Controller :: SetVertical() : {0}", value));

                // Set current & last vertical value...
                lastVerticalAxis = currentVerticalAxis;

                currentVerticalAxis = value;
            }

            // ***

            private void Steer()
            {
                steeringAngle = currentHorizontalAxis * maxSteerAngle;

                // Front wheels...
                frontLeftWheel.steerAngle = steeringAngle;
                frontRightWheel.steerAngle = steeringAngle;

                // Rear wheels...
                rearLeftWheel.steerAngle = steeringAngle * -1;
                rearRightWheel.steerAngle = steeringAngle * -1;
            }   

            private void Accelerate()
            {
                torque = currentVerticalAxis * GetTorque();

                // Setting each wheel torque...
                frontLeftWheel.motorTorque = torque;
                frontRightWheel.motorTorque = torque;

                // Middle wheels...
                middleLeftWheel.motorTorque = torque;
                middleRightWheel.motorTorque = torque;

                // Rear wheels...
                rearLeftWheel.motorTorque = torque;
                rearRightWheel.motorTorque = torque;
            } // TODO Break torque...

            private void UpdateWheelTransforms()
            {
                // Front wheels...
                UpdateWheelTransform(frontLeftWheel, frontLeftWheelTransform);
                UpdateWheelTransform(frontRightWheel, frontRightWheelTransform);
                
                // Middle wheels...
                UpdateWheelTransform(middleLeftWheel, middleLeftWheelTransform);
                UpdateWheelTransform(middleRightWheel, middleRightWheelTransform);
                
                // Rear wheels...
                UpdateWheelTransform(rearLeftWheel, rearLeftWheelTransform);
                UpdateWheelTransform(rearRightWheel, rearRightWheelTransform);
            }

            private void UpdateWheelTransform(WheelCollider wheel, Transform wheelTransform)
            {
                position = wheelTransform.position;
                rotation = wheelTransform.rotation;

                // Gets the world space pose of the wheel accounting for ground contact, suspension limits, steer angle, and rotation angle...
                wheel.GetWorldPose(out position, out rotation);

                // Set's wheel position and rotation...
                wheelTransform.position = position;
                wheelTransform.rotation = rotation;
            }

            // ***

            private float GetTorque()
            {
                return IsMovingFoward() ? motorForce : backwardMotorForce;
            }

            // ***

            private bool IsMovingFoward()
            {
                return currentVerticalAxis >= 0;
            }

            #endregion
        }
    }
}
