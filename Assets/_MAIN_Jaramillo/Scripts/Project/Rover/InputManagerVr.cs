///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 22/05/2019 08:00
///   Company: NMS
///-----------------------------------------------------------------

using System.Collections;
using UnityEngine;
using UnityEngine.Utils;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using VRTK;

namespace NMS
{
    namespace Rover
    {
        [RequireComponent(typeof(Controller))]
        public class InputManagerVr : MonoDebug
        {
            #region Properties

            [FoldoutGroup("Input Settings"), SerializeField, Space]
            private bool invertSteerAxis;
            [FoldoutGroup("Input Settings"), SerializeField]
            private bool invertVerticalAxis;

            [FoldoutGroup("Steer Values"), SerializeField, Range(0, 1), Space]
            private float steerGain;
            [FoldoutGroup("Steer Values"), SerializeField, Range(0, 1)]
            private float steerThreshold;

            [FoldoutGroup("Unity Events"), SerializeField, Space]
            private UnityEvent onRightControllerTriggerAxisChanged;
            [FoldoutGroup("Unity Events"), SerializeField]
            private UnityEvent onRightControllerTouchpadPressed;
            [FoldoutGroup("Unity Events"), SerializeField]
            private UnityEvent onLeftControllerTriggerAxisChanged;
            [FoldoutGroup("Unity Events"), SerializeField]
            private UnityEvent onLeftControllerTouchpadAxisChanged, onLeftControllerTouchpadTouchStart, onLeftControllerTouchpadTouchEnd;

            [FoldoutGroup("Others"), SerializeField, Space]
            private Vector2 axesOffset;

            // Hidden
            private float accelerateValue;
            private float desacelerateValue;

            private float? lastVerticalValue = null, currentVerticalValue = null;
            private float? lastSteerValue = null, currentSteerValue = null, steerTentativeValue;

            // Cached Components (Rover)
            private Controller roverController;
            private GateController gateController;

            // Cached Components (VRTK)
            private VRTK_ControllerEvents rightControllerEvents;
            private VRTK_ControllerEvents leftControllerEvents;

            // Coroutines
            private IEnumerator steerEndingCoroutine;

            #endregion

            #region Unity functions

            private void FixedUpdate()
            {
                UpdateVerticalAxis(GetVerticalAxisValue());
            }

            #endregion

            #region Class functions

            public override void GetComponents()
            {
                base.GetComponents();

                // Controllers...
                GetControllersEvents();
                
                // Rover components...
                GetRoverController();
                GetGateController();
            }

            public override void Suscribe()
            {
                base.Suscribe();

                // *** Right controller inputs...
                if (rightControllerEvents != null)
                {
                    // Acelerate...
                    rightControllerEvents.TriggerAxisChanged += OnRightControllerTriggerAxisChanged;
                    // Horn ...
                    rightControllerEvents.TouchpadPressed += OnRightControllerTouchpadPressed;
                }

                // *** Left controller inputs...
                if (leftControllerEvents != null)
                {
                    // Desacelerate...
                    leftControllerEvents.TriggerAxisChanged += OnLeftControllerTriggerAxisChanged;
                    // Steer axes...
                    leftControllerEvents.TouchpadAxisChanged += OnLeftControllerTouchpadAxisChanged;
                    leftControllerEvents.TouchpadTouchStart += OnLeftControllerTouchpadTouchStart;
                    leftControllerEvents.TouchpadTouchEnd += OnLeftControllerTouchpadTouchEnd;
                }
            }

            public override void Setup()
            {
                base.Setup();

                currentSteerValue = 0;

                gateController.Open();
            }
            
            // ***

            private void GetControllersEvents()
            {
                // Right controller events...
                rightControllerEvents = VRTK_FinderHelper.Instance.GetControllerEvents(VRTK_DeviceFinder.Devices.RightController);
                // Left controller events...
                leftControllerEvents = VRTK_FinderHelper.Instance.GetControllerEvents(VRTK_DeviceFinder.Devices.LeftController);
            }

            // ***

            private void GetRoverController()
            {
                roverController = GetComponent<Controller>();
            }

            private void GetGateController()
            {
                gateController = gameObject.GetOrAddComponent<GateController>();
            }

            // ***

            private void UpdateVerticalAxis(float? customValue = null)
            {
                // Set current & last vertical value...
                if (currentVerticalValue != null)
                    lastVerticalValue = currentVerticalValue;

                currentVerticalValue = EstimateCurrentVerticalAxisValue(customValue);
                
                // Function logs...
                Log(string.Format("InputManagerVr :: UpdateVerticalAxis() :: {0}", currentVerticalValue));
                
                // Set rover vertical axis...
                roverController.SetVertical((float)currentVerticalValue);
            }

            private void UpdateVerticalAxisAndInvokeEvent(UnityEvent unityEvent, float? customValue = null)
            {
                unityEvent?.Invoke();

                UpdateVerticalAxis(customValue);
            }

            private void UpdateSteerAxis(ControllerInteractionEventArgs eventArgs, float? customValue = null)
            {
                // Set current & last steer value...
                if (currentSteerValue != null)
                    lastSteerValue = currentSteerValue;

                steerTentativeValue = EstimateTentativeSteerAxisValue(eventArgs, customValue);

                // Adjusting current steer value...
                currentSteerValue = EstimateCurrentSteerAxisValue();

                // Function logs...
                Log(string.Format("InputManagerVr :: UpdateSteerAxis() :: {0}", currentSteerValue));

                // Set rover horizontal axis...
                roverController.SetHorizontal((float)currentSteerValue);
            }

            private void UpdateSteerAxisAndInvokeEvent(ControllerInteractionEventArgs eventArgs, UnityEvent unityEvent, float? customValue = null)
            {
                unityEvent?.Invoke();

                UpdateSteerAxis(eventArgs, customValue);
            }

            // ***

            private float? EstimateCurrentSteerAxisValue()
            {
                if (lastSteerValue != null)
                    return CheckSteerThreshold((float)steerTentativeValue, (float)lastSteerValue);
                else
                    return steerTentativeValue;
            }

            private float? EstimateCurrentVerticalAxisValue(float? customValue = null)
            {
                if (customValue == null)
                    return Mathf.Clamp(accelerateValue + desacelerateValue + axesOffset.y, -1, 1) * VerticalAxisDirection();
                else
                    return (customValue + axesOffset.y) * VerticalAxisDirection();
            }

            private float? EstimateTentativeSteerAxisValue(ControllerInteractionEventArgs eventArgs, float? customValue = null)
            {
                if (customValue == null)
                    return (eventArgs.touchpadAxis.x + axesOffset.x) * SteerAxisDirection();
                else
                    return (customValue + axesOffset.x) * SteerAxisDirection();
            }

            // ***

            private float SteerAxisDirection()
            {
                return (invertSteerAxis) ? -1 : 1;
            }

            private float VerticalAxisDirection()
            {
                return (invertVerticalAxis) ? -1 : 1;
            }

            // ***

            private float CheckSteerThreshold(float steerTentativeValue, float lastSteerValue)
            {
                if (Mathf.Abs(steerTentativeValue - lastSteerValue) <= steerThreshold)
                    return steerTentativeValue;
                else
                    return Mathf.Lerp(lastSteerValue, steerTentativeValue, steerGain);
            }

            // ***

            private void StartSteerEnding(ControllerInteractionEventArgs eventArgs)
            {
                StopSteerEnding();

                // Function logs...
                Log("InputManagerVr :: StartSteerEnding()");

                steerEndingCoroutine = SteerEnding(eventArgs);

                if (gameObject.activeInHierarchy)
                    StartCoroutine(steerEndingCoroutine);
            }

            private void StopSteerEnding()
            {
                if (steerEndingCoroutine != null)
                {
                    // Function logs...
                    Log("InputManagerVr :: StopSteerEnding()");

                    StopCoroutine(steerEndingCoroutine);

                    steerEndingCoroutine = null;
                }
            }

            // ***

            private float? GetVerticalAxisValue()
            {
                if (gateController != null)
                    return (gateController.isInRover) ? null : (float?) 0.0f;
                else
                    return null;
            }

            #endregion

            #region VRTK functions

            // Right controller events...
            private void OnRightControllerTriggerAxisChanged(object sender, ControllerInteractionEventArgs eventArgs)
            {
                Log(string.Format("InputManagerVr :: OnRightControllerTriggerAxisChanged() :: {0}", eventArgs.buttonPressure));

                accelerateValue = eventArgs.buttonPressure;

                onRightControllerTriggerAxisChanged?.Invoke();
            } // Acelerate...

            private void OnRightControllerTouchpadPressed(object sender, ControllerInteractionEventArgs eventArgs)
            {
                Log("InputManagerVr :: OnRightControllerTouchpadPressed()");

                onRightControllerTouchpadPressed?.Invoke();
            } // Horn...

            // Left controller events...
            private void OnLeftControllerTriggerAxisChanged(object sender, ControllerInteractionEventArgs eventArgs)
            {
                Log(string.Format("InputManagerVr :: OnLeftControllerTriggerAxisChanged() :: {0}", eventArgs.buttonPressure));

                desacelerateValue = eventArgs.buttonPressure * -1;

                onLeftControllerTriggerAxisChanged?.Invoke();
            } // Desacelerate...
                
            private void OnLeftControllerTouchpadAxisChanged(object sender, ControllerInteractionEventArgs eventArgs)
            {
                Log(string.Format("InputManagerVr :: OnLeftControllerTouchpadAxisChanged() :: {0}", eventArgs.touchpadAxis.x));

                OnLeftControllerTouchpadTouchEvent(eventArgs, onLeftControllerTouchpadAxisChanged);
            } // Steer axis changed...

            private void OnLeftControllerTouchpadTouchStart(object sender, ControllerInteractionEventArgs eventArgs)
            {
                Log(string.Format("InputManagerVr :: OnLeftControllerTouchpadTouchStart() :: {0}", eventArgs.touchpadAxis.x));

                StopSteerEnding();

                OnLeftControllerTouchpadTouchEvent(eventArgs, onLeftControllerTouchpadTouchStart);
            } // Steer axis start...

            private void OnLeftControllerTouchpadTouchEnd(object sender, ControllerInteractionEventArgs eventArgs)
            {
                Log(string.Format("InputManagerVr :: OnLeftControllerTouchpadTouchEnd() :: {0}", eventArgs.touchpadAxis.x));

                OnLeftControllerTouchpadTouchEvent(eventArgs, onLeftControllerTouchpadTouchEnd);

                StartSteerEnding(eventArgs);
            } // Steer axis end...

            // Left controller touchpad touch events template...
            private void OnLeftControllerTouchpadTouchEvent(ControllerInteractionEventArgs eventArgs, UnityEvent touchpadEvent)
            {
                if (gateController != null)
                {
                    if (gateController.isInRover)
                        UpdateSteerAxisAndInvokeEvent(eventArgs, touchpadEvent);
                }
                else
                    UpdateSteerAxisAndInvokeEvent(eventArgs, touchpadEvent);
            }

            #endregion

            #region Coroutines

            private IEnumerator SteerEnding(ControllerInteractionEventArgs eventArgs)
            {
                do
                {
                    UpdateSteerAxis(eventArgs, 0);

                    yield return null;

                } while (Mathf.Abs((float)currentSteerValue) > Mathf.Abs(axesOffset.x));
            }

            #endregion
        }
    }
}
