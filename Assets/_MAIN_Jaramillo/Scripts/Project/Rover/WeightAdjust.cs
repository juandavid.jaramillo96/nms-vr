///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 16/07/2019 17:00
///   Company: NMS
///-----------------------------------------------------------------

using UnityEngine;
using Sirenix.OdinInspector;

namespace NMS
{
    namespace Rover
    {
        [RequireComponent(typeof(Rigidbody))]
        public class WeightAdjust : MonoDebug
        {
            #region Properties

            [FoldoutGroup("Physics"), SerializeField, Space]
            private bool addAsOffset;
            [FoldoutGroup("Physics"), SerializeField]
            private Vector3 centerOfMass;

            // Getters & Setters
            public Vector3 currentCenterOfMass
            {
                get; private set;
            }

            public Vector3 lastCenterOfMass
            {
                get; private set;
            }

            // Cached Components
            private new Rigidbody rigidbody;

            #endregion

            #region Unity functions

            private void FixedUpdate()
            {
                CheckWeight();
            }

            #endregion

            #region Class functions

            public override void GetComponents()
            {
                base.GetComponents();

                GetRigidbody();
            }

            public override void Setup()
            {
                base.Setup();

                CheckWeight();
            }

            // ***

            private void GetRigidbody()
            {
                rigidbody = GetComponent<Rigidbody>();
            }

            // ***

            private void CheckWeight()
            {
                if (centerOfMass != currentCenterOfMass)
                {
                    // Setting current & last steer mass...
                    lastCenterOfMass = currentCenterOfMass;

                    currentCenterOfMass = centerOfMass;

                    // Logs center of mass...
                    Log(string.Format("WeightAdjust :: CheckWeight() :: Original COM :: {0}", rigidbody.centerOfMass));

                    // Setting rigidbody center of mass...
                    if (addAsOffset)
                        rigidbody.centerOfMass += currentCenterOfMass;
                    else
                        rigidbody.centerOfMass = currentCenterOfMass;

                    Log(string.Format("WeightAdjust :: CheckWeight() :: Current COM :: {0}", rigidbody.centerOfMass));
                }
            }

            #endregion
        }
    }
}
