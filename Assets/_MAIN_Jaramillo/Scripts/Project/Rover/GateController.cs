///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 21/05/2019 08:51
///   Company: NMS
///-----------------------------------------------------------------

using UnityEngine;
using Sirenix.OdinInspector;
using VRTK;

namespace NMS
{
    namespace Rover
    {
        [RequireComponent(typeof(Controller))]
        public class GateController : MonoDebug
        {
            #region Properties

            [FoldoutGroup("VRTK"), SerializeField, Space]
            private VRTK_PolicyList policyList;

            // Getters & Setters
            public bool isInRover
            {
                get; private set;
            }

            // Cached Components (Rover)
            private Controller roverController;

            // Cached Components (VRTK)
            private VRTK_ControllerEvents rightControllerEvents;
            private VRTK_ControllerEvents leftControllerEvents;

            private VRTK_Pointer rightPointer;
            private VRTK_Pointer leftPointer;

            #endregion

            #region Class functions

            public override void GetComponents()
            {
                base.GetComponents();

                // Controllers...
                GetControllersEvents();
                GetPointers();
            }

            public override void Suscribe()
            {
                base.Setup();

                // Controllers events...
                if (leftControllerEvents != null)
                    leftControllerEvents.ButtonTwoPressed += OnLeftControllerButtonTwoPressed;
            }

            // ***

            private void GetControllersEvents()
            {
                // Right controller events...
                rightControllerEvents = VRTK_FinderHelper.Instance.GetControllerEvents(VRTK_DeviceFinder.Devices.RightController);
                // Left controller events...
                leftControllerEvents = VRTK_FinderHelper.Instance.GetControllerEvents(VRTK_DeviceFinder.Devices.LeftController);
            }

            private void GetPointers()
            {
                // Right pointer...
                rightPointer = VRTK_FinderHelper.Instance.GetPointer(VRTK_DeviceFinder.Devices.RightController);
                // Left pointer...
                leftPointer = VRTK_FinderHelper.Instance.GetPointer(VRTK_DeviceFinder.Devices.LeftController);
            }

            // ***

            public void Open()
            {
                Log("EnterExitControllerVr :: Enter()");

                if (isInRover)
                    return;

                SwitchState();
            }

            public void Close()
            {
                Log("EnterExitControllerVr :: Exit()");

                if (!isInRover)
                    return;

                SwitchState();
            }

            // ***

            private void SwitchState()
            {
                isInRover = !isInRover;

                // Toggle pojnters off...
                SetPointers(!isInRover);
            }

            // ***

            private void SetPointer(VRTK_Pointer pointer, bool state)
            {
                if (pointer != null)
                {
                    if (!state)
                        pointer.Toggle(state);

                    pointer.enabled = state;
                }
            }

            private void SetPointers(bool state)
            {
                Log(string.Format("EnterExitControllerVr :: SetPointers() :: {0}", state.ToString()));

                SetPointer(rightPointer, state);
                SetPointer(leftPointer, state);
            }

            #endregion

            #region VRTK functions

            private void OnLeftControllerButtonTwoPressed(object sender, ControllerInteractionEventArgs eventArgs)
            {
                Log("EnterExitControllerVr :: OnLeftControllerButtonTwoPressed()");

                SwitchState();
            }

            #endregion
        }
    }
}
