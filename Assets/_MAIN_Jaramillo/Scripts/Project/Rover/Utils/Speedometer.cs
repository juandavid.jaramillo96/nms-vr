///----------------s-------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 19/07/2019 14:23
///   Company: NMS
///-----------------------------------------------------------------

using UnityEngine;

public class Speedometer : MonoDebug
{
    #region Properties

    [Space, SerializeField]
    private Rigidbody target;

    // Getters & Setters
    public float speed
    {
        get
        {
            return (target != null) ? target.velocity.magnitude : 0.0f;
        }
    }

	#endregion

	#region Unity functions

    private void Update()
    {
        if (target != null)
            Log(string.Format("Speedometer :: speed :: {0}", speed));
    }

	#endregion
	
	#region Class functions

	public override void GetComponents()
	{
        if (target == null)
            target = GetComponent<Rigidbody>();
	}

	#endregion
}
