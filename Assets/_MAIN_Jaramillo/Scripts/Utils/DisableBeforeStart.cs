///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 27/12/2018 09:32
///-----------------------------------------------------------------

public class DisableBeforeStart : MonoDebug
{
    #region Unity functions

    protected override void Awake()
    {
        base.Awake();

        Disable();
    }

    #endregion

    #region Class functions

    private void Disable()
    {
        Log("DisableBeforeStart :: Disable()");

        gameObject.SetActive(false);
    }

    #endregion
}