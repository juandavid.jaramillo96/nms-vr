///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 28/12/2018 11:38
///-----------------------------------------------------------------

namespace UnityEngine
{
    namespace Utils
    {
        public class DeletePlayerPrefKeys : MonoBehaviour
        {
            #region Properties

            [Space, SerializeField]
            private bool deleteAll;

            [Space, SerializeField]
            private string[] keysToDelete;

            #endregion

            #region Unity functions

            private void Start()
            {
                DeleteAll();
            }

            #endregion

            #region Class functions

            private void DeleteAll()
            {
                if (deleteAll)
                    PlayerPrefs.DeleteAll();

                if (keysToDelete.Length > 0)
                {
                    foreach (var key in keysToDelete)
                    {
                        if (PlayerPrefs.HasKey(key))
                            PlayerPrefs.DeleteKey(key);
                    }
                }
            }

            #endregion
        }
    }
}
