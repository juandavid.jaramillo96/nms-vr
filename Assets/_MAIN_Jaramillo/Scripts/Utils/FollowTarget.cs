///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 13/11/2018 14:48
///-----------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoDebug
{
    #region Properties

    [Space, SerializeField]
    protected Transform target;

    [Space, SerializeField]
    protected bool updateXPosition;
    [SerializeField]
    protected bool updateYPosition, updateZPosition;

    [Space, SerializeField]
    protected bool updateXRotation;
    [SerializeField]
    protected bool updateYRotation, updateZRotation;

    [Space, SerializeField]
    protected bool keepConstantX;
    [SerializeField]
    protected bool keepConstantY, keepConstantZ;

    [Space, SerializeField]
    protected float distance;
    [SerializeField]
    protected float altitude;

    [Space, SerializeField]
    protected Vector3 positionOffset;
    [SerializeField]
    protected Vector3 rotationOffset;

    [Header("Smooth Options"), SerializeField]
    protected float smoothTime;
    [SerializeField]
    protected float smoothFactor;

    [Header("Others"), SerializeField]
    protected bool updateFromStart;

    // Hidden
    protected Vector3 velocity = Vector3.zero;

    // Cached Components
    protected new Transform transform;

    protected float newXPos, newYPos, newZPos;
    protected float newXRot, newYRot, newZRot;

    protected Vector3 newPosition;
    protected Vector3 newRotation;

    // Coroutines
    public IEnumerator followTarget
    {
        get; protected set;
    }

    #endregion

    #region Class functions

    public override void GetComponents()
    {
        transform = GetComponent<Transform>();
    }

    public override void Setup()
    {
        smoothFactor = (smoothFactor != 0) ? smoothFactor : 1;

        if (updateFromStart)
            StartFollowTarget();
    }

    // ***

    public void StartFollowTarget()
    {
        StopFollowTarget();

        Log("VRFollowTarget :: StartFollowTarget()");

        followTarget = Follow();

        StartCoroutine(followTarget);
    }

    public void StopFollowTarget()
    {
        if (followTarget != null)
        {
            Log("VRFollowTarget :: StopFollowTarget()");

            StopCoroutine(followTarget);
        }
    }

    #endregion

    #region Coroutines

    protected virtual IEnumerator Follow()
    {
        while (isActiveAndEnabled)
        {
            // Check properties
            if (!updateXPosition && !keepConstantX)
                keepConstantX = true;

            if (!updateXPosition && !keepConstantX)
                keepConstantX = true;

            if (!updateXPosition && !keepConstantX)
                keepConstantX = true;

            // Updates target position...
            newXPos = (updateXPosition) ? target.position.x : transform.position.x;
            newXPos += positionOffset.x;

            if (!keepConstantX)
                newXPos += ((target.forward.x * distance) + (target.up.x * altitude));

            newYPos = (updateYPosition) ? target.position.y : transform.position.y;
            newYPos += positionOffset.y;

            if (!keepConstantY)
                newYPos += ((target.forward.y * distance) + (target.up.y * altitude) + positionOffset.y);

            newZPos = (updateZPosition) ? target.position.z : transform.position.z;
            newZPos += positionOffset.z;

            if (!keepConstantZ)
                newZPos += ((target.forward.z * distance) + (target.up.z * altitude) + positionOffset.z);

            // Updates target rotation...
            newXRot = (updateXRotation) ? target.eulerAngles.x : transform.eulerAngles.x;
            newXRot += rotationOffset.x;

            newYRot = (updateYRotation) ? target.eulerAngles.y : transform.eulerAngles.y;
            newYRot += rotationOffset.y;

            newZRot = (updateZRotation) ? target.eulerAngles.z : transform.eulerAngles.z;
            newZRot += rotationOffset.z;

            // Setting new position...
            newPosition = new Vector3(
                newXPos,
                newYPos,
                newZPos);

            // Setting new rotation...
            newRotation = new Vector3(
                newXRot,
                newYRot,
                newZRot);

            // Setting transform position...
            transform.position = Vector3.SmoothDamp(transform.position, newPosition, ref velocity, smoothTime, Mathf.Infinity, (Time.deltaTime * smoothFactor));

            // Setting transform rotation...
            transform.rotation = Quaternion.Euler(newRotation);

            yield return null;
        }
    } // Pure Math!

    #endregion
}
