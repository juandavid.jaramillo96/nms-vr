///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 27/12/2018 09:32
///-----------------------------------------------------------------

using UnityEngine;

public class OffsetMovement : MonoDebug
{
    #region Properties

    [Space, SerializeField]
    private float scrollSpeed;
    [SerializeField]
    private Renderer[] renderers;

    // Getters & Setters
    public float currentScrollSpeed
    {
        get; private set;
    }
    public float lastScrollSpeed
    {
        get; private set;
    }

    // Hidden
    private Vector2 offset;

    #endregion

    #region Unity functions

    private void Update()
    {
        if (lastScrollSpeed != currentScrollSpeed)
            lastScrollSpeed = currentScrollSpeed;

        currentScrollSpeed = Time.time * scrollSpeed;

        UpdateOffset(new Vector2(0, currentScrollSpeed));
    }

    #endregion

    #region Class functions

    public override void GetComponents()
    {
        if (renderers == null)
            renderers = GetComponentsInChildren<Renderer>();
    }

    // ***

    private void UpdateOffset(Vector2 offset)
    {
        if (renderers != null)
        {
            Log(string.Format("OffsetMovement :: UpdateOffset() :: {0}", offset));

            foreach (var renderer in renderers)
            {
                renderer.material.SetTextureOffset("_MainTex", offset);
            }
        }
    }

    #endregion
}
