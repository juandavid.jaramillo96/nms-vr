///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 27/12/2018 09:32
///-----------------------------------------------------------------

using UnityEngine;

/// <summary>
/// Class for log debug purposes on unity console
/// </summary>
public abstract class MonoDebug : Sirenix.OdinInspector.SerializedMonoBehaviour
{
    #region Properties

    [SerializeField]
    public bool enableConsoleLog;

    #endregion

    #region Unity functions

    protected virtual void Awake()
    {
        GetComponents();

        Suscribe();
    }

    protected virtual void Start()
    {
        Setup();
    }

    #endregion

    #region Class functions

    public virtual void GetComponents()
    {
        // ...
    }

    public virtual void Suscribe()
    {
        // ...
    }

    public virtual void Setup()
    {
        // ...
    }

    // ***

    protected virtual void Log(string message)
    {
        if (enableConsoleLog)
            Debug.Log(message);
    }

    protected virtual void LogWarning(string message)
    {
        if (enableConsoleLog)
            Debug.LogWarning(message);
    }

    protected virtual void LogError(string message)
    {
        if (enableConsoleLog)
            Debug.LogError(message);
    }

    #endregion
}
