///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 27/12/2018 09:32
///-----------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Facade class that manage the load scene events of the game.
/// </summary>
public class SceneLoader : Sirenix.OdinInspector.SerializedMonoBehaviour
{
    #region Properties

    [SerializeField]
    private bool _loadOnce;

    [Space, SerializeField]
    private HashSet<int> scenesToLoadWithLoadingScreen;
    [SerializeField]
    private GameObject loadingScreenPrefab;

    // Getters & Setters
    public int sceneToLoad
    {
        get; private set;
    }

    public int sceneToUnload
    {
        get; private set;
    }

    /// <summary>
    /// Property that allows you know the current scene index.
    /// </summary>
    [HideInInspector]
    public int actualScene
    {
        get; private set;
    }

    /// <summary>
    /// Property that allows you know the progress of the actual scene loading.
    /// </summary>
    [HideInInspector]
    public float loadProgress
    {
        get; private set;
    }

    /// <summary>
    /// Property that allows you know if the actual scene loading has completed.
    /// </summary>
    [HideInInspector]
    public bool isDone
    {
        get; private set;
    }

    /// <summary>
    /// Property that allos you load only one scene at the same time.
    /// </summary>
    [HideInInspector]
    public bool loadOnce
    {
        get
        {
            return _loadOnce;
        }
        set
        {
            _loadOnce = value;
        }
    }

    /// <summary>
    /// Controls Additive or Single scene load mode
    /// </summary>
    [HideInInspector]
    public LoadSceneMode loadSceneMode
    {
        get; private set;
    }

    // Hidden
    private bool isLoadingOtherScene;

    /// <summary>
    /// Static property that performs like singleton.
    /// </summary>
    public static SceneLoader Instance
    {
        get; private set;
    }

    #endregion

    #region Unity functions

    private void Awake()
    {
        if (Instance != null)
            DestroyImmediate(this);
        else
            Instance = this;

        DontDestroyOnLoad(this);
    }

    #endregion

    #region Class functions

    /// <summary>
    /// Function that loads a specific scene.
    /// </summary>
    /// <param name="sceneToLoad">Used to point the scene index at the build settings.</param>
    public void LoadScene(int sceneToLoad)
    {
        LoadScene(sceneToLoad, LoadSceneMode.Single);
    }

    /// <summary>
    /// Function that loads a specific scene.
    /// </summary>
    /// <param name="sceneToLoad">Used to point the scene index at the build settings.</param>
    public void LoadSceneAdditive(int sceneToLoad)
    {
        LoadScene(sceneToLoad, LoadSceneMode.Additive);
    }

    /// <summary>
    /// Function that unload a specific scene.
    /// </summary>
    /// <param name="sceneToUnload"></param>
    public void UnloadScene(int sceneToUnload)
    {
        if (sceneToUnload >= 0 && sceneToUnload < SceneManager.sceneCountInBuildSettings)
        {
            this.sceneToUnload = sceneToUnload;
        }
        else
        {
            Debug.LogError("This scene doesn't exist.");
            return;
        }

        StartCoroutine(UnloadScene());
    }

    private void LoadScene(int sceneToLoad, LoadSceneMode loadSceneMode)
    {
        SetLoadSceneMode(loadSceneMode);

        if (sceneToLoad >= 0 && sceneToLoad < SceneManager.sceneCountInBuildSettings)
        {
            this.sceneToLoad = sceneToLoad;
        }
        else
        {
            Debug.LogError("This scene doesn't exist.");
            return;
        }

        StartCoroutine(LoadScene());
    }

    // ***

    /// <summary>
    /// Function that loads the next scene in the build settings.
    /// </summary>
    /// <remarks>
    /// If you are at the top scene in build settings, this function will going to load the first scene in the build settings.
    /// </remarks>
    public void LoadNextScene()
    {
        LoadNextScene(LoadSceneMode.Single);
    }

    /// <summary>
    /// Function that loads the next scene in the build settings.
    /// </summary>
    /// <remarks>
    /// If you are at the top scene in build settings, this function will going to load the first scene in the build settings.
    /// </remarks>
    public void LoadNextSceneAdditive()
    {
        LoadNextScene(LoadSceneMode.Additive);
    }

    private void LoadNextScene(LoadSceneMode loadSceneMode)
    {
        if (!SceneChecker())
            return;

        SetLoadSceneMode(loadSceneMode);

        switch (SceneManager.GetActiveScene().buildIndex < SceneManager.sceneCountInBuildSettings - 1)
        {
            case true:
                sceneToLoad = SceneManager.GetActiveScene().buildIndex + 1;
                break;

            case false:
                LoadFirstOne();
                return;
        }

        StartCoroutine(LoadScene());
    }

    // ***

    /// <summary>
    /// Function that loads the previous scene in the build settings.
    /// </summary>
    /// <remarks>
    /// If you are at the bottom scene in build settings, this function will going to load the last scene in the build settings.
    /// </remarks>
    public void LoadPreviousScene()
    {
        LoadPreviousScene(LoadSceneMode.Single);
    }

    /// <summary>
    /// Function that loads the previous scene in the build settings.
    /// </summary>
    /// <remarks>
    /// If you are at the bottom scene in build settings, this function will going to load the last scene in the build settings.
    /// </remarks>
    public void LoadPreviousSceneAdditive()
    {
        LoadPreviousScene(LoadSceneMode.Additive);
    }

    private void LoadPreviousScene(LoadSceneMode loadSceneMode)
    {
        if (!SceneChecker())
            return;

        SetLoadSceneMode(loadSceneMode);
        
        switch (SceneManager.GetActiveScene().buildIndex > 0)
        {
            case true:
                sceneToLoad = SceneManager.GetActiveScene().buildIndex - 1; ;
                break;

            case false:
                LoadLastOne();
                return;
        }

        StartCoroutine(LoadScene());
    }

    // ***

    /// <summary>
    /// Function that loads the first scene in the build settings.
    /// </summary>
    public void LoadFirstScene()
    {
        LoadFirstScene(LoadSceneMode.Single);
    }

    /// <summary>
    /// Function that loads the first scene in the build settings.
    /// </summary>
    public void LoadFirstSceneAdditive()
    {
        LoadFirstScene(LoadSceneMode.Additive);
    }

    private void LoadFirstScene(LoadSceneMode loadSceneMode)
    {
        if (!SceneChecker())
            return;

        SetLoadSceneMode(loadSceneMode);

        LoadFirstOne();
    }

    // ***

    /// <summary>
    /// Function that loads the last scene in the build settings.
    /// </summary>
    public void LoadLastScene()
    {
        LoadLastScene(LoadSceneMode.Single);
    }

    /// <summary>
    /// Function that loads the last scene in the build settings.
    /// </summary>
    public void LoadLastSceneAdditive()
    {
        LoadLastScene(LoadSceneMode.Additive);
    }

    private void LoadLastScene(LoadSceneMode loadSceneMode)
    {
        if (!SceneChecker())
            return;

        SetLoadSceneMode(loadSceneMode);

        LoadLastOne();
    }

    // ***

    /// <summary>
    /// Function that reloads the current scene.
    /// </summary>
    public void ReloadScene()
    {
        sceneToLoad = SceneManager.GetActiveScene().buildIndex;

        StartCoroutine(LoadScene());
    }

    // ***

    private void LoadFirstOne()
    {
        sceneToLoad = 0;

        StartCoroutine(LoadScene());
    }

    private void LoadLastOne()
    {
        sceneToLoad = SceneManager.sceneCountInBuildSettings - 1;

        StartCoroutine(LoadScene());
    }

    private bool SceneChecker()
    {
        switch (SceneManager.sceneCountInBuildSettings)
        {
            case 0:
                CaseNoScenes();
                return false;

            case 1:
                CaseOneScene();
                return false;

            default:
                return true;
        }
    }

    private void CaseNoScenes()
    {
        Debug.LogError("There's not scenes in build settings.");
    }

    private void CaseOneScene()
    {
        Debug.LogWarning("There's only one scene in build settings.");

        ReloadScene();
    }

    // ***

    private void SetLoadSceneMode(LoadSceneMode loadSceneMode)
    {
        if (this.loadSceneMode != loadSceneMode)
            this.loadSceneMode = loadSceneMode;
    }

    // ***

    private void SetLoadingScreen()
    {
        if (loadingScreenPrefab != null)
        {
            if (scenesToLoadWithLoadingScreen.Contains(sceneToLoad))
                Instantiate(loadingScreenPrefab);
        }
    }

    #endregion

    #region Coroutines

    private IEnumerator LoadScene()
    {
        if (loadOnce && isLoadingOtherScene)
        {
            StopAllCoroutines();

            yield return null;
        }

        AsyncOperation loadScene = SceneManager.LoadSceneAsync(sceneToLoad, loadSceneMode);

        SetLoadingScreen();

        // ...

        isDone = false;

        isLoadingOtherScene = !isDone;

        while (!loadScene.isDone)
        {
            loadProgress = loadScene.progress / 0.9f;

            yield return null;
        }

        isDone = true;

        isLoadingOtherScene = !isDone;

        // ...

        actualScene = SceneManager.GetActiveScene().buildIndex;
    }

    private IEnumerator UnloadScene()
    {
        if (loadOnce && isLoadingOtherScene)
        {
            StopAllCoroutines();

            yield return null;
        }

        AsyncOperation loadScene = SceneManager.UnloadSceneAsync(sceneToUnload);

        isDone = false;
        isLoadingOtherScene = !isDone;

        while (!loadScene.isDone)
        {
            loadProgress = loadScene.progress / 0.9f;

            yield return null;
        }

        isDone = true;
        isLoadingOtherScene = !isDone;

        actualScene = SceneManager.GetActiveScene().buildIndex;
    }

    #endregion
}
