///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 27/12/2018 09:32
///-----------------------------------------------------------------

using UnityEngine;

[ExecuteInEditMode]
public class ImageEffectFade : ImageEffect
{
    #region Properties

    [SerializeField, Range(0,1)]
    protected float fadeColor = 1;
    
    // Hidden
    protected float lastFadeColor = -1;

    #endregion

    #region Unity functions

    protected override void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (lastTexelSize != texelSize)
            MainMaterial.SetFloat("_TexelSize", texelSize);

        if (lastFadeColor != fadeColor)
            MainMaterial.SetFloat("_FadeColor", fadeColor);

        Graphics.Blit(source, destination, MainMaterial);

        lastTexelSize = texelSize;

        lastFadeColor = fadeColor;
    }

    #endregion

    #region Class functions

    public void SetFadeColor(float fadeColor)
    {
        this.fadeColor = fadeColor;
    }

    #endregion
}
