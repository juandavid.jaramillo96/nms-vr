///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 27/12/2018 09:32
///-----------------------------------------------------------------

using UnityEngine;

[ExecuteInEditMode]
public class ImageEffect : MonoDebug
{
    #region Properties

    public Shader shader;

    public float texelSize = 1f;

    // Hidden
    protected float lastTexelSize = -1;

    // Getters & Setters
    protected Material MainMaterial
    {
        get
        {
            if (mainMaterial != null)
            {
                return mainMaterial;
            }
            else
            {
                mainMaterial = new Material(shader);
                mainMaterial.hideFlags = HideFlags.HideAndDontSave;
            }

            return mainMaterial;
        }
    }

    // Cached Components
    protected Material mainMaterial;

    #endregion

    #region Unity Functions

    protected void OnEnable()
    {
        // ...
    }

    protected void OnDisable()
    {
        if (mainMaterial != null)
            DestroyImmediate(mainMaterial);
    }

    protected virtual void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (lastTexelSize != texelSize)
            MainMaterial.SetFloat("_TexelSize", texelSize);

        Graphics.Blit(source, destination, MainMaterial);

        lastTexelSize = texelSize;
    }

    #endregion

    #region Class functions

    public override void Setup()
    {
        if (!SystemInfo.supportsImageEffects)
        {
            enabled = false;
            return;
        }

        if (!shader || !shader.isSupported)
        {
            enabled = false;
            return;
        }
    }

    #endregion
}
