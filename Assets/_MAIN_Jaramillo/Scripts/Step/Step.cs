///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 16/04/2019 11:01
///-----------------------------------------------------------------

using System;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

[Serializable]
public class Step
{
    #region Properties

    public string stepName;

    [Space, SerializeField, FoldoutGroup("Step")]
    private UnityEvent onStart, onEnd;

    #endregion

    #region Class functions

    public void Execute()
    {
        Debug.Log("Step :: Execute()");

        if (onStart != null)
            onStart.Invoke();
    }

    public void Stop()
    {
        Debug.Log("Step :: Stop()");

        if (onEnd != null)
            onEnd.Invoke();
    }

    #endregion
}
