///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 16/04/2019 10:56
///-----------------------------------------------------------------

using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

public class StepManager : MonoDebug, IProcess
{
    #region Properties

    [Space, SerializeField]
    private Step[] steps;

    [Space, SerializeField, FoldoutGroup("On Start")]
    private UnityEvent onStart;
    [SerializeField, FoldoutGroup("On End")]
    private UnityEvent onEnd;

    [Space, SerializeField]
    private bool executeOnStart;

    // Getters & Setters
    public bool isInProcess
    {
        get; private set;
    }

    public Step currentStep
    {
        get; private set;
    }
    public Step lastStep
    {
        get; private set;
    }

    // Hidden
    private int currentStepIndex;

    // Cached Components
    private IProcess process;

    // Coroutines
    private IEnumerator nextLastCoroutine;

    #endregion

    #region Class functions

    public override void GetComponents()
	{
        process = GetComponent<IProcess>();
    }

    public override void Setup()
    {
        if (executeOnStart)
            ProcessStart();
    }

    // ***

    public void Next()
    {
        if (!isInProcess)
            return;

        Log("StepManager :: Next()");

        // Index increment...
        currentStepIndex++;

        if (currentStep != null)
            currentStep.Stop();

        // Step execution...
        TriggerStep(currentStepIndex);
    }

    public void Next(float delay)
    {
        if (nextLastCoroutine != null)
            StopCoroutine(nextLastCoroutine);

        nextLastCoroutine = WaitForNext(delay);

        StartCoroutine(nextLastCoroutine);
    }

    public void Last()
    {
        if (!isInProcess)
            return;

        Log("StepManager :: Last()");

        // Index increment...
        currentStepIndex--;

        if (currentStep != null)
            currentStep.Stop();

        // Step execution...
        TriggerStep(currentStepIndex);
    }

    public void Last(float delay)
    {
        if (nextLastCoroutine != null)
            StopCoroutine(nextLastCoroutine);

        nextLastCoroutine = WaitForLast(delay);

        StartCoroutine(nextLastCoroutine);
    }

    // ***

    private void SetCurrentStep(int currentStepIndex)
    {
        // Setting current & last step...
        if (currentStep != null)
            lastStep = currentStep;

        currentStep = steps[currentStepIndex];
    }

    // ***

    private void TriggerStep(int currentStepIndex)
    {
        CheckCurrentStepIndex(currentStepIndex);

        // Setting current & last step...
        SetCurrentStep(currentStepIndex);

        // Step execution...
        Log(string.Format("StepManager :: TriggerStep() :: {0}", currentStepIndex));

        currentStep.Execute();
    }

    // ***

    private void CheckCurrentStepIndex(int currentStepIndex)
    {
        currentStepIndex %= steps.Length;

        this.currentStepIndex = currentStepIndex;
    }

    // ***

    public void ProcessStart()
    {
        process.Start();
    }

    public void ProcessEnd()
    {
        process.End();
    }

    #endregion

    #region Interfaces implementations

    void IProcess.Start()
    {
        Log("StepManager :: IProcess.Start()");

        // Checking if is a currrent event running...
        if (currentStep != null)
            currentStep.Stop();

        currentStep = null;

        // Setting last index and new current index...
        if (currentStepIndex != 0)
            currentStepIndex = 0;

        // Event...
        if (onStart != null)
            onStart.Invoke();

        isInProcess = true;

        // Triggered...
        TriggerStep(currentStepIndex);
    }

    void IProcess.End()
    {
        Log("StepManager :: IProcess.End()");

        // Checking if is a currrent event running...
        if (currentStep != null)
            currentStep.Stop();

        currentStep = null;

        // Setting last index and new current index...
        if (currentStepIndex != 0)
            currentStepIndex = 0;

        // Event...
        if (onEnd != null)
            onEnd.Invoke();

        isInProcess = false;
    }

    #endregion

    #region Coroutines

    private IEnumerator WaitForNext(float delay)
    {
        yield return new WaitForSeconds(delay);

        Next();
    }

    private IEnumerator WaitForLast(float delay)
    {
        yield return new WaitForSeconds(delay);

        Last();
    }

    #endregion
}
