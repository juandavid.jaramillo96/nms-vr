///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 13/06/2019 13:01
///-----------------------------------------------------------------

using System;
using UnityEngine;
using UnityEngine.Events;

public class TriggerListener : MonoDebug
{
    #region Properties

    [Space, SerializeField]
    private DynamicColliderEvent onTriggerEnter;
    [SerializeField]
    private DynamicColliderEvent onTriggerStay, onTriggerExit;

    // Events
    [HideInInspector]
    public Action<Collider> onTriggerEnterAction, onTriggerStayAction, onTriggerExitAction; // TODO Use 'Event' instead of 'UnityEvent' and 'Action'

    #endregion

    #region Unity functions

    private void OnTriggerEnter(Collider collider)
    {
        Log(string.Format("TriggerListener :: OnTriggerEnter() :: {0}", collider.name));

        onTriggerEnter?.Invoke(collider);
        onTriggerEnterAction?.Invoke(collider);
    }

    private void OnTriggerStay(Collider collider)
    {
        Log(string.Format("TriggerListener :: OnTriggerStay() :: {0}", collider.name));

        onTriggerStay?.Invoke(collider);
        onTriggerStayAction?.Invoke(collider);
    }

    private void OnTriggerExit(Collider collider)
    {
        Log(string.Format("TriggerListener :: OnTriggerExit() :: {0}", collider.name));

        onTriggerExit?.Invoke(collider);
        onTriggerExitAction?.Invoke(collider);
    }

    #endregion
}
