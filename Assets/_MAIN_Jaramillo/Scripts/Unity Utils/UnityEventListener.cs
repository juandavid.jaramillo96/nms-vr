///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 17/10/2018 16:01
///-----------------------------------------------------------------

using UnityEngine;
using UnityEngine.Events;

public class UnityEventListener : MonoBehaviour
{
    #region Properties

    [Space, SerializeField]
    private UnityEvent onEnable, onDisable, onAwake, onStart, onUpdate, onFixedUpdate, onLateUpdate; // TODO Use 'Event' instead of 'UnityEvent' and 'Action'

    #endregion

    #region Unity functions

    private void OnEnable()
    {
        onEnable?.Invoke();
    }

    private void OnDisable()
    {
        onDisable?.Invoke();
    }

    private void Awake()
    {
        onAwake?.Invoke();
    }

    private void Start()
    {
        onStart?.Invoke();
    }

    private void Update()
    {
        onUpdate?.Invoke();
    }

    private void FixedUpdate()
    {
        onFixedUpdate?.Invoke();
    }

    private void LateUpdate()
    {
        onLateUpdate?.Invoke();
    }

    #endregion
}
