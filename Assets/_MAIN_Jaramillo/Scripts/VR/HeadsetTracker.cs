///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 13/11/2018 14:48
///-----------------------------------------------------------------

using UnityEngine;
using UnityEngine.Events;
using Valve.VR;

public class HeadsetTracker : MonoDebug
{
    #region Properties

    [SerializeField]
    protected UnityEvent onHeadsetOn, onHeadsetOff; // TODO Use 'Event' instead of 'UnityEvent' and 'Action'

    #endregion

    #region Unity functions

    private void Update()
    {
        try
        {
            if (SteamVR_Controller.Input(0).GetPress(EVRButtonId.k_EButton_ProximitySensor))
            {
                Log("VRHeadsetTracker :: OnHeadsetOn");

                onHeadsetOn?.Invoke();
            }
            else
            {
                Log("VRHeadsetTracker :: OnHeadsetOff");

                onHeadsetOff?.Invoke();
            }
        }
        catch (System.Exception)
        {
            LogWarning("An error has occurred");
        }
    }

    #endregion
}
