///-----------------------------------------------------------------
///   Author : Santiago Cardona                    
///   Date   : 05/07/2019 09:14
///   Company: NEDIAR
///-----------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public abstract class AudioManager : MonoDebug
{
    #region Properties

    [FoldoutGroup("Sounds"), SerializeField, Space]
    protected List<AudioSource> sounds = new List<AudioSource>();

    // Getters & Setters
    public List<AudioSource> Sounds
    {
        get
        {
            return sounds;
        }
    }

    // Cached Components
    protected float cachedVolume;

    protected int currentSound;
    protected AudioSource currentSource;

    protected Dictionary<AudioSource, float> initialVolumeValues = new Dictionary<AudioSource, float>();

    #endregion // Properties

    #region Unity functions

    private void Awake()
	{
		GetComponents();

		Suscribe();
	}

    private void Start()
	{
		Setup();
	}

    #endregion // Unity functions

    #region Class functions

    protected virtual void GetComponents()
	{
		// ...
	}

	protected virtual void Suscribe()
	{
        // ...	
    }

    protected virtual void Setup()
	{
        SetSoundsVolumeDictionary();
    }

    // ***

    protected void SetSoundsVolumeDictionary()
    {
        initialVolumeValues = sounds.ToDictionary(sound => sound, sound => sound.volume);
    }

    // ***

    public void Play(AudioSource source)
    {
        if (sounds.Contains(source))
        {
            Log(string.Format("AudioManager :: Play() :: {0}", source.name));

            if (source.isPlaying)
                source.Stop();

            source.Play();
        }
    }

    public void Play(int sound)
    {
        currentSource = sounds[sound];

        Play(currentSource);
    }

    // ***

    public void TryPlay(AudioSource source)
    {
        if (sounds.Contains(source))
        {
            Log(string.Format("AudioManager :: TryPlay() :: {0}", source.name));

            if (!source.isPlaying)
                source.Play();
        }
    }

    public void TryPlay(int sound)
    {
        currentSource = sounds[sound];

        TryPlay(currentSource);
    }

    // ***

    public void LerpPlay(AudioSource source, float startVolume, float finalVolume, float time, Action callback = null, LeanTweenType curve = LeanTweenType.linear)
    {
        if (sounds.Contains(source))
        {
            Log(string.Format("AudioManager :: LerpPlay() :: {0}", source.name));

            // Creates a callback if there's not a delegate passed as argument...
            if (callback == null)
                callback = delegate () { };

            if (source.isPlaying)
                source.Stop();

            source.Play();

            // Lean tween transition...
            LeanTween.value(gameObject: gameObject, from: startVolume, to: finalVolume, time: time, callOnUpdate: (volume) => source.volume = volume).setEase(curve).setOnComplete(callback);
        }
    }

    public void LerpPlay(int sound, float startVolume, float finalVolume, float time, Action callback = null, LeanTweenType curve = LeanTweenType.linear)
    {
        currentSource = sounds[sound];

        LerpPlay(currentSource, startVolume, finalVolume, time, callback, curve);
    }

    // ***

    public void Stop(AudioSource source)
    {
        if (sounds.Contains(source))
        {
            Log(string.Format("AudioManager :: Stop() :: {0}", source.name));

            if (source.isPlaying)
                source.Stop();
        }
    }

    public void Stop(int sound)
    {
        currentSource = sounds[sound];

        Stop(currentSource);
    }

    // ***

    public void TryStop(AudioSource source)
    {
        if (sounds.Contains(source))
        {
            Log(string.Format("AudioManager :: TryStop() :: {0}", source.name));

            if (source.isPlaying)
                source.Stop();
        }
    }

    public void TryStop(int sound)
    {
        currentSource = sounds[sound];

        TryStop(currentSource);
    }

    // ***

    public void LerpStop(AudioSource source, float startVolume, float finalVolume, float time, Action callback = null, LeanTweenType curve = LeanTweenType.linear)
    {
        if (sounds.Contains(source))
        {
            Log(string.Format("AudioManager :: LerpStop() :: {0}", source.name));

            // Creates a callback if there's not a delegate passed as argument...
            if (callback == null)
                callback = delegate () { };

            callback += source.Stop;

            // Lean tween transition...
            if (source.isPlaying)
                LeanTween.value(gameObject: gameObject, from: startVolume, to: finalVolume, time: time, callOnUpdate: (volume) => source.volume = volume).setEase(curve).setOnComplete(callback);
        }
    }

    public void LerStop(int sound, float startVolume, float finalVolume, float time, Action callback = null, LeanTweenType curve = LeanTweenType.linear)
    {
        currentSource = sounds[sound];

        LerpStop(currentSource, startVolume, finalVolume, time, callback, curve);
    }

    // ***

    public void SetVolume(AudioSource source, float volume)
    {
        if (sounds.Contains(source))
        {
            Log(string.Format("AudioManager :: SetVolume() :: {0} :: {1}", source.name, volume));

            if (source.volume != volume)
                source.volume = volume;
        }
    }

    public void SetVolume(int sound, float value)
    {
        currentSource = sounds[sound];

        SetVolume(currentSource, value);
    }

    // ***

    public void SetPitch(AudioSource source, float pitch)
    {
        if (sounds.Contains(source))
        {
            Log(string.Format("AudioManager :: SetPitch() :: {0} :: {1}", source.name, pitch));

            if (source.pitch != pitch)
                source.pitch = pitch;
        }
    }

    public void SetPitch(int sound, float value)
    {
        currentSource = sounds[sound];

        SetPitch(currentSource, value);
    }

    // ***

    public void SetLoop(AudioSource source, bool loop)
    {
        if (sounds.Contains(source))
        {
            Log(string.Format("AudioManager :: SetLoop() :: {0} :: {1}", source.name, loop));

            if (source.loop != loop)
                source.loop = loop;
        }
    }

    public void SetLoop(int sound, bool loop)
    {
        currentSource = sounds[sound];

        SetLoop(currentSource, loop);
    }

    // ***

    protected void StopLerping(int sound, float lerpTime = 1)
    {
        // Stopping sound...
        LerpStop(
            sounds[sound],
            sounds[sound].volume, 0,
            lerpTime,
            curve: LeanTweenType.easeOutCubic);
    }

    // ***

    protected void CheckSoundVolume(int sound)
    {
        // Getting current source...
        currentSource = sounds[sound];

        // If the source is not playing, it set's source volume to hsi initial value...
        if (currentSource != null)
        {
            if (!currentSource.isPlaying)
                SetVolume(currentSource, initialVolumeValues[sounds[sound]]);
        }

    }

    #endregion // Class functions

    #region Coroutines

    public IEnumerator WaitUntilSoundEnd(int sound, Action callback)
    {
        do
        {
            yield return null;

        } while (sounds[sound].isPlaying);

        // Event execution...
        callback?.Invoke();
    }

    #endregion // Coroutines
}
