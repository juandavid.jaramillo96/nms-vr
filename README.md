# NMS-VR

NMS-VR is a Virtual Reality solution for the 'Eeny, Meeney, Miney, Sample!' problem, from the **NASA Space Apps Challenge** 2019

## The Challenge
You are the astronaut/robotic mission lead tasked with bringing valuable specimens from the Moon back to Earth for further study. <br />
How will you evaluate lunar samples quickly and effectively before or while still on the mission? <br />
How will you differentiate samples of potential scientific value from less interesting material?

## Background
Between 1969 and 1972, Apollo astronauts brought back 800 pounds of lunar rocks from six landing sites. How did they know what was the best rock to bring home?
When two people walk along the beach, do they both pick up the same type of sea shells or fragments from the same shell? <br />

All lunar samples may have some type of value, but some have more scientific importance than others, and the notion of “value” really depends on the interests of the scientist or engineer.
With very limited time and tools to pick up and assess the rocks they gather, how can scientists get the most scientific and engineering “value” from astronauts’ time on the Moon’s surface?
Some scientists and/or engineers care about the historical record of the Moon that tells a long story of the origins of our solar system. Some care about the potential for water.
Some want to study how meteorites on Earth compare to lunar rock. Some want to build structures on the Moon and therefore need to know the properties of the soil.
Many more competing priorities are relevant to today’s scientists and engineers, but in past missions there has been no way to assess the samples during the mission before leaving the Moon to give clear priorities as to which samples or parts of samples to bring home. <br />

The goal of this challenge is to help increase the scientific and engineering value of each human lunar mission by assessing lunar samples in-situ before or during the mission and only collecting those samples or parts of samples that are of highest value to the specific mission.

## Installation
Go to the 'Releases' section of the project, and download the lastest version.

## Contributors
![](https://66.media.tumblr.com/8fe8e7eb772230a2803e6ce831ff30b6/tumblr_pzloddBRmk1y4r7kfo1_250.png)
![](https://66.media.tumblr.com/de3dc887810763edd96582ea630ea6c2/tumblr_pzloddBRmk1y4r7kfo4_250.png)
![](https://66.media.tumblr.com/809a2f37045762012133bb2779c13693/tumblr_pzloddBRmk1y4r7kfo5_250.png)
![](https://66.media.tumblr.com/d9a03080d191e82dda9eae8fb0cfa26a/tumblr_pzloddBRmk1y4r7kfo3_250.png)
![](https://66.media.tumblr.com/2145199e90df35bc2fc4e2b326f4ca9a/tumblr_pzloddBRmk1y4r7kfo6_250.png)
![](https://66.media.tumblr.com/ec76ca9ff395b96eaf169e8f2714e4cb/tumblr_pzloddBRmk1y4r7kfo2_250.png)
![](https://66.media.tumblr.com/78a005f13ecd7c808de03672583e4bea/tumblr_pzloddBRmk1y4r7kfo7_250.png)

* Andrés Rodríguez, *Technical Artist*
* Esteban Jiménez, *Programmer*
* Juan David Jaramillo, *Programmer*
* Juan David Barrea, *Programmer*
* José Alejandro Bedoya, *Researcher*
* Edward Stiven Rodríguez, *Researcher*
* Santiago Cardona, *Creative Director*

*Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change. Please make sure to update tests as appropriate.*

## License
[MIT](https://choosealicense.com/licenses/mit/)
